from django.shortcuts import render,redirect

# Create your views here.
from .models import Provincia
from .models import Ingrediente

def index(request):
    return render(request,'index.html')




#PROVINCIAS
def listadoProvincias(request):
    return render(request,'listadoProvincias.html')

from django.contrib import messages

def listadoProvincias(request):
    provinciasBdd=Provincia.objects.all()
    return render(request,'listadoProvincias.html',{'provincias':provinciasBdd})


def guardarProvincia(request):
    #Capturando los valores del formulario por POST
    id_prov_gb=request.POST["id_prov_gb"]
    nombre_prov_gb=request.POST["nombre_prov_gb"]
    capital_prov_gb=request.POST["capital_prov_gb"]
    poblacion_prov_gb=request.POST["poblacion_prov_gb"]

    #Insertando datos mediante el ORM de DJANGO
    nuevoProvincia=Provincia.objects.create(nombre_prov_gb=nombre_prov_gb,capital_prov_gb=capital_prov_gb,poblacion_prov_gb=poblacion_prov_gb)
    messages.success(request,'Provincia guardada exitosamente')
    return redirect('/')
    #llamadno al ORM, Capturando
def eliminarProvincia(request,id):
    provinciaEliminar=Provincia.objects.get(id_prov_gb=id)
    provinciaEliminar.delete()
    messages.success(request,'Provincia Eliminada Exitosamente')
    return redirect('/')

def editarProvincias(request,id):
    provinciasEditar=Provincia.objects.get(id_prov_gb=id)
    return render(request,
    'editarProvincias.html',{'provincia':provinciasEditar})
    #Actualizar

def procesarActualizacionProvincia(request):
    id_prov_gb=request.POST["id_prov_gb"]
    nombre_prov_gb=request.POST["nombre_prov_gb"]
    capital_prov_gb=request.POST["capital_prov_gb"]
    poblacion_prov_gb=request.POST["poblacion_prov_gb"]


    #Insertando datos mediante el ORM de DJANGO
    provinciaEditar=Provincia.objects.get(id_prov_gb=id_prov_gb)
    provinciaEditar.nombre_prov_gb=nombre_prov_gb
    provinciaEditar.capital_prov_gb=capital_prov_gb
    provinciaEditar.poblacion_prov_gb=poblacion_prov_gb

    provinciaEditar.save()
    messages.success(request,
      'Provincia Actualizada Exitosamente')
    return redirect('/')



# TABLA INGREDIENTE ___________________________________________________________________________________________________

#PROVINCIAS
def listadoIngredientes(request):
    return render(request,'listadoIngredientes.html')

from django.contrib import messages

def listadoIngredientes(request):
    ingredientesBdd=Ingrediente.objects.all()
    return render(request,'listadoIngredientes.html',{'ingredientes':ingredientesBdd})


def guardarIngrediente(request):
    #Capturando los valores del formulario por POST
    id_ingre_gb=request.POST["id_ingre_gb"]
    nombre_ingre_gb=request.POST["nombre_ingre_gb"]
    tipo_ingre_gb=request.POST["tipo_ingre_gb"]
    fechain_ingre_gb=request.POST["fechain_ingre_gb"]
    fechacad_ingre_gb=request.POST["fechacad_ingre_gb"]

    #Insertando datos mediante el ORM de DJANGO
    nuevoIngrediente=Ingrediente.objects.create(nombre_ingre_gb=nombre_ingre_gb,tipo_ingre_gb=tipo_ingre_gb,fechain_ingre_gb=fechain_ingre_gb,fechacad_ingre_gb=fechacad_ingre_gb)
    messages.success(request,'Ingrediente guardado exitosamente')
    return redirect('listadoIngredientes')
    #llamadno al ORM, Capturando
def eliminarIngrediente(request,id):
    ingredienteEliminar=Ingrediente.objects.get(id_ingre_gb=id)
    ingredienteEliminar.delete()
    messages.success(request,'Ingrediente Eliminado Exitosamente')
    return redirect('listadoIngredientes')

def editarIngredientes(request,id):
    ingredientesEditar=Ingrediente.objects.get(id_ingre_gb=id)
    return render(request,
    'editarIngredientes.html',{'ingrediente':ingredientesEditar})
    #Actualizar

def procesarActualizacionIngrediente(request):
    id_ingre_gb=request.POST["id_ingre_gb"]
    nombre_ingre_gb=request.POST["nombre_ingre_gb"]
    tipo_ingre_gb=request.POST["tipo_ingre_gb"]
    fechain_ingre_gb=request.POST["fechain_ingre_gb"]
    fechacad_ingre_gb=request.POST["fechacad_ingre_gb"]


    #Insertando datos mediante el ORM de DJANGO
    ingredienteEditar=Ingrediente.objects.get(id_ingre_gb=id_ingre_gb)
    ingredienteEditar.nombre_ingre_gb=nombre_ingre_gb
    ingredienteEditar.tipo_ingre_gb=tipo_ingre_gb
    ingredienteEditar.fechain_ingre_gb=fechain_ingre_gb
    ingredienteEditar.fechacad_ingre_gb=fechacad_ingre_gb


    ingredienteEditar.save()
    messages.success(request,
      'Ingrediente Actualizado Exitosamente')
    return redirect('/')
