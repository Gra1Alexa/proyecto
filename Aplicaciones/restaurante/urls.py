from django.urls import path
from . import views
from .views import index


urlpatterns=[
    path('',index,name='index'),

    path('listadoProvincias/',views.listadoProvincias, name='listadoProvincias'),
    path('guardarProvincia/',views.guardarProvincia),
    path('eliminarProvincia/<id>',views.eliminarProvincia),
    path('editarProvincias/<id>',views.editarProvincias),
    path('procesarActualizacionProvincia/',views.procesarActualizacionProvincia),

    path('listadoIngredientes/',views.listadoIngredientes, name='listadoIngredientes'),
    path('guardarIngrediente/',views.guardarIngrediente),
    path('eliminarIngrediente/<id>',views.eliminarIngrediente),
    path('editarIngredientes/<id>',views.editarIngredientes),
    path('procesarActualizacionIngrediente/',views.procesarActualizacionIngrediente),



]
