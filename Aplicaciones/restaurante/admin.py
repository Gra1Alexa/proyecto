from django.contrib import admin
from .models import Provincia
from .models import Ingrediente

# Register your models here.
admin.site.register(Provincia)
admin.site.register(Ingrediente)

