from django.db import models

# Create your models here.

class Provincia(models.Model):
    id_prov_gb=models.AutoField(primary_key=True)
    nombre_prov_gb=models.CharField(max_length=150)
    capital_prov_gb=models.CharField(max_length=100)
    poblacion_prov_gb=models.IntegerField()
    def __str__(self):
        fila="{0}:{1} {2} {3}"
        return fila.format(self.id_prov_gb,self.nombre_prov_gb,self.capital_prov_gb,self.poblacion_prov_gb)


class Ingrediente(models.Model):
	id_ingre_gb=models.AutoField(primary_key=True)
	nombre_ingre_gb=models.CharField(max_length=100)
	tipo_ingre_gb=models.CharField(max_length=100)
	fechain_ingre_gb=models.DateField()
	fechacad_ingre_gb=models.DateField()
	def __str__(self):
		fila = "{0}: {1} {2} {3}" 
		return fila.format(self.id_ingre_gb,self.nombre_ingre_gb,self.tipo_ingre_gb,self.fechain_ingre_gb,self.fechacad_ingre_gb)